Examples
========

.. module:: gymbag

Record
------

.. literalinclude:: ../examples/record.py
    :caption: Record environment data to a file, then read it back in.

Playback
--------

.. literalinclude:: ../examples/playback.py
    :caption: Create an environment that will play back recorded data.

Record to List
--------------

.. literalinclude:: ../examples/record_list.py
    :caption: Record environment data to a list in memory, then play it back.

Agent Playback
--------------

.. literalinclude:: ../examples/playback_agent.py
    :caption: An agent that plays back actions, ignoring the environment.

Random Environment
------------------

.. literalinclude:: ../examples/playback_random.py
    :caption: A :class:`RandomEnv <gymbag.core.RandomEnv>` will generate random observations and rewards.

Metadata
--------
.. literalinclude:: ../examples/metadata.py
    :caption: Reading file-level metadata.
