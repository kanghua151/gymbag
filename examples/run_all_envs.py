#!/usr/bin/env python
"""Run every environment possible, testing recording and playback."""

import sys
import os

import gym

from gymbag import record_hdf5, HDF5Reader
from gymbag.core import drive_env, set_done
from tests.test_core import assert_episodes_equal

MAX_EPISODES = 2
MAX_EPISODE_STEPS = 3
CLEANUP = True


def quiet_remove(filename):
    """rm -f"""
    try:
        os.remove(filename)
    except IOError:
        pass


def main():
    """Try to record all known environments to HDF5."""
    all_specs = sorted(gym.envs.registry.all(), key=lambda s: s.id)
    for spec_num, spec in enumerate(all_specs, 1):
        print('{:4d}/{}: {}'.format(spec_num, len(all_specs), spec.id))
        filename = spec.id + '.h5'
        quiet_remove(filename)
        try:
            env = record_hdf5(spec.make(), filename)
            # env = spec.make()
            # Run env for MAX_EPISODES, each MAX_EPISODE_STEPS, put actual data in a list
            episodes = list(map(list, drive_env(env, episodes=MAX_EPISODES, steps_per_episode=min(spec.max_episode_steps or MAX_EPISODE_STEPS, MAX_EPISODE_STEPS))))
        except Exception as exc:        # pylint: disable=broad-except
            print('Skipping {}: {}'.format(spec.id, exc), file=sys.stderr)
            quiet_remove(filename)
            continue

        # drive_env() may not mark last step done, but recorder needs to, so ensure last step is done
        for steps in episodes:
            steps[-1] = set_done(steps[-1])

        # Read data from HDF5, compare to list
        actual = list(map(list, HDF5Reader(filename)))
        assert_episodes_equal(episodes, actual)

        if CLEANUP:
            os.remove(filename)


if __name__ == '__main__':
    main()
