#!/usr/bin/env python
"""Simple example recording CartPole data to HDF5."""
# pylint: disable=invalid-name

import gym
from gymbag import record_hdf5, HDF5Reader

# Wrap env with HDF5 recorder, specify filename
env = record_hdf5(gym.make('CartPole-v0'), 'cartpole.h5')
for episode in range(2):
    env.reset()
    done = False
    while not done:
        obs, reward, done, info = env.step(env.action_space.sample())
env.close()     # Closes the file

# Read it back
for episode in HDF5Reader('cartpole.h5'):
    for time, obs, action, reward, done, info in episode:
        print(time, obs, action, reward, done, info)
