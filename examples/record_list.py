#!/usr/bin/env python
"""Record OpenAI Gym data to a list with Gymbag."""
# pylint: disable=invalid-name

import gym

from gymbag import ListRecorder, RecordEnv, PlaybackEnv

# Make a RecordEnv with a ListRecorder and wrap the CartPole env
recorder = ListRecorder()
env = RecordEnv(gym.make('CartPole-v0'), recorder)
for episode in range(2):
    env.reset()
    done = False
    while not done:
        obs, reward, done, info = env.step(env.action_space.sample())

env.close()

# Iterate over the data directly
for episode in recorder.data:
    for time, obs, action, reward, done, info in episode:
        print(time, obs, action, reward, done, info)


# Play the data back in an environment
playback_env = PlaybackEnv(recorder.data, env.observation_space, env.action_space)
while not playback_env.played_out:
    playback_env.reset()
    done = False
    while not done:
        obs, reward, done, info = playback_env.step(playback_env.action_space.sample())
        print(obs, reward, done, info)

env.close()
