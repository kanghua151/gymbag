#!/usr/bin/env python
"""Gymbag metadata example.  Uses cartpole data from record.py."""
# pylint: disable=invalid-name

from gymbag import HDF5Reader

reader = HDF5Reader('cartpole.h5')
print('Table:', reader.name)
print('Description:', reader.description)
print('Total Steps:', reader.nsteps)
print('Observation Space:', reader.observation_space)
print('Action Space:', reader.action_space)
reader.close()
