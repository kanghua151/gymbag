#!/usr/bin/env python
"""Tests for Gymbag HDF5 support."""

# Copyright (C) 2017  Doctor J
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import unittest
import itertools
import warnings
from tempfile import NamedTemporaryFile
from typing import Any

import gym
import numpy as np

from gymbag.core import generate_random, record_from_iter, RandomEnv, null_sample, DONE, drive_env
from gymbag.hdf5 import HDF5Recorder, HDF5Reader, record_hdf5
from .test_core import OBSERVATION_SPACES, ACTION_SPACES, assert_episodes_equal


class HDF5Test(unittest.TestCase):
    # pylint: disable=invalid-name,missing-docstring
    def test_HDF5Recorder(self) -> None:
        for obs_space, act_space in itertools.product(OBSERVATION_SPACES, ACTION_SPACES):
            # NamedTemporaryFile can't be portably closed and re-opened, so this will fail on Windows.
            with NamedTemporaryFile(prefix='test_HDF5Recorder', suffix='.h5') as file:
                recorder = HDF5Recorder[Any, Any](file.name, obs_space, act_space, description='Test Data')
                episodes = list(map(list, generate_random(obs_space.sample, act_space.sample)))
                record_from_iter(recorder, episodes)

                with warnings.catch_warnings():     # We don't bother to nullify the first steps
                    warnings.simplefilter("ignore", UserWarning)
                    reader = HDF5Reader(file.name)
                    self.assertEqual(reader.observation_space, obs_space)
                    self.assertEqual(reader.action_space, act_space)
                    self.assertEqual(reader.nsteps, sum(len(ep) for ep in episodes))
                    assert_episodes_equal(reader, episodes, places=6)

    def test_HDF5Recorder_append(self) -> None:
        obs_space, act_space = OBSERVATION_SPACES[0], ACTION_SPACES[0]
        with NamedTemporaryFile(prefix='test_HDF5Recorder_append', suffix='.h5') as file:
            recorder1 = HDF5Recorder[Any, Any](file.name, obs_space, act_space, description='Test Data')
            episodes1 = list(map(list, generate_random(obs_space.sample, act_space.sample)))
            record_from_iter(recorder1, episodes1)

            recorder2 = HDF5Recorder[Any, Any](file.name, obs_space, act_space, description='Test Data')
            episodes2 = list(map(list, generate_random(obs_space.sample, act_space.sample)))
            record_from_iter(recorder2, episodes2)

            with warnings.catch_warnings():     # We don't bother to nullify the first steps
                warnings.simplefilter("ignore", UserWarning)
                reader = HDF5Reader(file.name)
                self.assertEqual(reader.observation_space, obs_space)
                self.assertEqual(reader.action_space, act_space)
                self.assertEqual(reader.nsteps, sum(len(ep) for ep in itertools.chain(episodes1, episodes2)))
                assert_episodes_equal(reader, itertools.chain(episodes1, episodes2), places=6)

    def test_HDF5Recorder_reset(self) -> None:
        # Test calling reset() before the env signals done
        obs_space, act_space = OBSERVATION_SPACES[0], ACTION_SPACES[0]
        env = RandomEnv[Any, Any](obs_space, act_space, episode_steps=10)
        with NamedTemporaryFile(prefix='test_HDF5Recorder_reset', suffix='.h5') as file:
            env = record_hdf5(env, file.name, 'Test Data')
            expected = []
            for episode in range(10):
                done = False
                expected.append([[0, env.reset(), null_sample(act_space), np.nan, False, None]])
                while not done and len(expected[-1]) <= episode:
                    action = env.action_space.sample()
                    obs, reward, done, info = env.step(action)
                    expected[-1].append([0, obs, action, reward, done, info])
                expected[-1][-1][DONE] = True       # Recorder should ensure last step is done=True, even if passed done=False
            env.close()

            with warnings.catch_warnings():     # We don't bother to nullify the first steps
                warnings.simplefilter("ignore", UserWarning)
                reader = HDF5Reader(file.name)
                self.assertEqual(reader.observation_space, obs_space)
                self.assertEqual(reader.action_space, act_space)
                self.assertEqual(reader.nsteps, sum(len(ep) for ep in expected))
                assert_episodes_equal(expected, reader, ignore_time=True, places=6)

    def test_hdf5(self) -> None:
        for obs_space, act_space in itertools.product(OBSERVATION_SPACES, ACTION_SPACES):
            env = RandomEnv[Any, Any](obs_space, act_space, episode_steps=10)
            with NamedTemporaryFile(prefix='test_hdf5', suffix='.h5') as file:
                env = record_hdf5(env, file.name)
                expected = []
                for episode in range(5):        # pylint: disable=unused-variable
                    done = False
                    expected.append([(0, env.reset(), null_sample(act_space), np.nan, False, None)])
                    while not done:
                        action = env.action_space.sample()
                        obs, reward, done, info = env.step(action)
                        expected[-1].append((0, obs, action, reward, done, info))
                env.close()

                with warnings.catch_warnings():     # We don't bother to nullify the first steps
                    warnings.simplefilter("ignore", UserWarning)
                    reader = HDF5Reader(file.name)
                    self.assertEqual(reader.observation_space, obs_space)
                    self.assertEqual(reader.action_space, act_space)
                    self.assertEqual(reader.nsteps, sum(len(ep) for ep in expected))
                    assert_episodes_equal(expected, reader, ignore_time=True, places=6)

    def test_hdf5_metadata(self) -> None:
        desc = 'Gymbag Test Data'
        with NamedTemporaryFile(prefix='test_hdf5_metadata', suffix='.h5') as file:
            env = gym.make('CartPole-v0')
            wrapped = record_hdf5(env, file.name, description=desc)
            expected = list(map(list, drive_env(wrapped, episodes=2, steps_per_episode=10)))
            wrapped.close()

            reader = HDF5Reader(file.name)
            self.assertEqual(reader.name, env.spec.id)
            self.assertEqual(reader.description, desc)
            self.assertEqual(reader.observation_space, env.observation_space)
            self.assertEqual(reader.action_space, env.action_space)
            self.assertEqual(reader.nsteps, sum(sum(1 for _ in ep) for ep in expected))
            reader.close()
