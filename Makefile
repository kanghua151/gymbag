export PYTHONPATH = .
EXAMPLE_OUTPUT = gymbag_example_output.txt
.PHONY: all test unittest intest pylint mypy docs dist disttest clean

all: test pylint mypy docs

test: unittest intest

unittest:
	python -m unittest -v

intest:
	# Order matters, so we can't just use a loop.
	rm -f $(EXAMPLE_OUTPUT)
	examples/record.py >> $(EXAMPLE_OUTPUT)
	examples/playback.py >> $(EXAMPLE_OUTPUT)
	examples/record_list.py >> $(EXAMPLE_OUTPUT)
	examples/playback_random.py >> $(EXAMPLE_OUTPUT)
	examples/playback_agent.py >> $(EXAMPLE_OUTPUT)
	# cat $(EXAMPLE_OUTPUT)
	rm -f cartpole.h5 $(EXAMPLE_OUTPUT)

slowtest:
	examples/performance.py
	examples/run_all_envs.py

pylint:
	pylint --rcfile .pylintrc gymbag examples tests

mypy:
	mypy --disallow-untyped-defs --strict-optional gymbag

docs:
	$(MAKE) -C docs html

dist:
	./dist.sh https://upload.pypi.org/legacy/ https://pypi.org

disttest:
	./dist.sh https://test.pypi.org/legacy/ https://test.pypi.org

clean:
	python setup.py clean --all
	rm -rf dist/ gymbag.egg-info/ gymbag-*.tar.gz
	$(MAKE) -C docs clean
